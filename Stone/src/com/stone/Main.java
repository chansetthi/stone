package com.stone;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        boolean validateChar = true;
        boolean validateInput = true;
        String inputChar;
        List<Character> charRand = new ArrayList<>();
        System.out.println("Input");
        int number = scanner.nextInt();
        scanner.nextLine();
        if (number < 2 ){
            System.out.println("Please enter a number greater than 1.");
            return;
        }
        int length = 0;
        do {
            validateChar = true;
            inputChar = scanner.nextLine();
            length = inputChar.length();
            if (length != number){
                System.out.println("Please enter character must be equal " + number + " character");
                System.out.println("Input");
                continue;
            }
            charRand.clear();
            for (int i = 0; i < number; i++) {
                if(inputChar.charAt(i) != 'R' && inputChar.charAt(i) != 'G' && inputChar.charAt(i) != 'B'){
                    System.out.println("Please enter character is \"R\" \"G\" \"B\" " + number + " character");
                    validateChar = false;
                    break;
                }
                charRand.add(inputChar.charAt(i));
            }
            if (length == number &&  validateChar){
                validateInput = false;
            }

        }
        while (validateInput);
        for (int i = 0; i < number - 1; i++) {
            if (charRand.get(i).equals(charRand.get(i+1))){
                count ++;
            }
        }
        System.out.println("Output");
        System.out.println(count);

    }
}
